<?php
header('Access-Control-Allow-Origin: *');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require '../vendor/autoload.php';
require 'config.php';

function getCurl($url){
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: wwwrd"));
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $output = curl_exec($ch);
  curl_close($ch);
  return $output;
}

$app = new \Slim\App(['settings' => $config]);

$app->get('/', function () {
    $name = "/q";
    return $name;
});

$app->get('/q', function (Request $request, Response $response, array $args) {
  //var_dump($_GET);
  $mongoip = $this->get('settings')['mongo_server'];
  //$response->getBody()->write("Source IP $srcip");
  $manager = new MongoDB\Driver\Manager("mongodb://$mongoip");
  //Query
  //date
  $from = isset($_GET['from'])?strtotime($_GET['from'])*1000:"";
  if($from){
    $to = (isset($_GET['to']) && $_GET['to'])?strtotime($_GET['to'])*1000:$from+86400*1000;
    $filter["time"] = ['$gte' => new \MongoDB\BSON\UTCDatetime($from), '$lte' => new \MongoDB\BSON\UTCDatetime($to)];
  }
  //source ip
  if(isset($_GET['srcip']) && $_GET['srcip']){
    $filter["srcip1"] =  $_GET['srcip'];
  }
  if(isset($_GET['srcp']) && $_GET['srcp']){
    $filter["srcp"] =  $_GET['srcp'];
  }
  if(isset($_GET['dstip']) && $_GET['dstip']){
    $filter["dst"] =  $_GET['dstip'];
  }
  if(isset($_GET['dstp']) && $_GET['dstp']){
    $filter["dstp"] =  $_GET['dstp'];
  }
  //limit skip
  $limit = isset($_GET["limit"])?$_GET["limit"]:$this->get('settings')['limit'];
  $page = isset($_GET["page"])?$_GET["page"]:"0";
  $skip = $page*$limit;
  $options = ["limit" => $limit,"skip" => $skip];
  $query = new MongoDB\Driver\Query($filter, $options);
  $cursor = $manager->executeQuery('syslog.node1', $query);
  foreach($cursor as $doc){
    $time1 = $doc->time->__toString();
    $doc->timestamp = $time1/1000;
    $result[] = $doc;
  }
  if(!isset($result)){
    return "[]";
  }
  return $response->withJson($result);
});

$app->get('/u', function (Request $request, Response $response, array $args) {
  //var_dump($_GET);
  $mongoip = $this->get('settings')['mongo_server'];
  //$response->getBody()->write("Source IP $srcip");
  $manager = new MongoDB\Driver\Manager("mongodb://$mongoip");
  //Query
  //date
  $from = isset($_GET['from'])?strtotime($_GET['from'])*1000:"";
  if($from){
    $to = (isset($_GET['to']) && $_GET['to'])?strtotime($_GET['to'])*1000:$from+86400*1000;
    $filter["time"] = ['$gte' => new \MongoDB\BSON\UTCDatetime($from), '$lte' => new \MongoDB\BSON\UTCDatetime($to)];
  }
  //source ip
  if(isset($_GET['srcip']) && $_GET['srcip']){
    $filter["srcip1"] =  $_GET['srcip'];
  }
  if(isset($_GET['srcp']) && $_GET['srcp']){
    $filter["srcp"] =  $_GET['srcp'];
  }
  if(isset($_GET['dstip']) && $_GET['dstip']){
    $filter["dst"] =  $_GET['dstip'];
  }
  if(isset($_GET['dstp']) && $_GET['dstp']){
    $filter["dstp"] =  $_GET['dstp'];
  }
  //limit skip
  $limit = isset($_GET["limit"])?$_GET["limit"]:$this->get('settings')['limit'];
  $page = isset($_GET["page"])?$_GET["page"]:"0";
  $skip = $page*$limit;
  $options = ["limit" => $limit,"skip" => $skip];
  $query = new MongoDB\Driver\Query($filter, $options);
  $cursor = $manager->executeQuery('syslog.node1', $query);
  foreach($cursor as $doc){
    $time1 = $doc->time->__toString();
    $doc->timestamp = $time1/1000;
    $response1 = getCurl("http://wwwrd/q?srcip=10.211.65.220&from=2014-03-18T12:00:00&limit=1");
    $json = json_decode($response1);
    $user = $json[0]->{'Calling-Station-Id'};
    $doc->username = $user;
    $result[] = $doc;
  }
  if(!isset($result)){
    return "[]";
  }
  return $response->withJson($result);
});
$app->run();
