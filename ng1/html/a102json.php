<?php
$stdin = fopen('php://stdin', 'r');
$stderr = fopen('php://stderr', 'w');
$reg = '/([A-Z][a-z]* +[^ ]* [^ ]*) ([^ ]*).* ([^:]+:[^:]+:[^ ]+) ([0-9\.]+) ([^ ]*) - . \[(TCP|UDP|ICMP|RTP) ([^:]+):([^\ ]+) ([^\:]+):([^ ]+) ([^:]+):([^ ]+) ([^:]+):([^ ]+) \"?([^\"]+|[\-])\"? .*\]/';
$count = 0;
$Errcount = 0;
if(!isset($argv[1])){
	echo "a102json.php [uniq keyid]";
}else{
	$tag = $argv[1];
}

$now = time();
$last_y = date("Y")-1; //fix bug
while(!feof($stdin)){
	$line = fgets($stdin);
	$line = trim($line);
	if(!$line){
		continue;
	}
	$match = preg_match($reg,$line,$r);
	if($match){
		//echo $line."\n";
		//var_dump($r);
		$datetime = $r[1];
		//`echo '$datetime' >> /tmp/debug`;
		$ts = strtotime($datetime);
		//fix bug 'Feb 19 19:29' dont know year
		//change Feb 19 19:29 to 2013-02-19 19:29:00
		if($now < $ts){
			$date_tmp = date("-m-d H:i:s",$ts);
			$date_tmp = $last_y.$date_tmp;
			$ts = strtotime($date_tmp);
		}
		$ts = $ts."000";
		$a10 = $r[2];
		$ntype = $r[6];
		$src = $r[7];
		$srcp = $r[8];
		$dst = $r[11];
		$dstp = $r[12];
		$natip =  $r[13];
		$natp = $r[14];
		$msisdn = $r[15];
		$uri = "";
		$json = '{"a10" : "'.$a10.'", "ntype" : "'.$ntype.'", "srcip1" : "'.$src.'", "srcp" : "'.$srcp.'", "dst" : "'.$dst.'", "dstp" : "'.$dstp.'", "natip" : "'.$natip.'", "natp" : "'.$natp.'", "msisdn" : "'.$msisdn.'", "uri":"'.$uri.'" ,"time" : { "$date" : '.$ts.' } }'."\n";
		echo $json;
		$count++;
 		//`echo '$json' >> /tmp/debug`;
	}else{
		$Errcount++;
		//`echo 'not match $line' >> /tmp/debug`;
		fwrite($stderr,"($Errcount)not match $line\n");
	}
	//die();
}
?>
