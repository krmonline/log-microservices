package main
//Version 0.1
//Create by Chakrit<chakrit@softnix.co.th>
import (
	"fmt"
	"time"
	"github.com/confluent-kafka-go/kafka"
	"github.com/go-zookeeper/zk"
	"github.com/tidwall/gjson"
	"strings"
	"bufio"
	"os"
	"io"
	"flag"

)

func getPartition(topic string) []string {
  c, _, err := zk.Connect([]string{"zookeeper"}, time.Second) //*10)
  if err != nil {
    panic(err)
  }
  path := "/brokers/topics/"+topic+"/partitions"
  children, _, _ , err := c.ChildrenW(path)
  if err != nil {
    panic(err)
  }
  //fmt.Println(children)
  return children
}

func getBroker() []string {
  c, _, err := zk.Connect([]string{"zookeeper"}, time.Second) //*10)
  if err != nil {
    panic(err)
  }
  path := "/brokers/ids"
  children, _, _ , err := c.ChildrenW(path)
  if err != nil {
    panic(err)
  }
  results := make([]string,len(children))
  
  for key, element := range children {
    id_path := path+"/"+element
    //fmt.Printf("%+v\n", id_path)
    result_byte,_,err := c.Get(id_path)
    if err != nil {
      panic(err)
    }
    json_kafkaid := string(result_byte[:])
    value := gjson.Get(json_kafkaid, "endpoints.0")
    hostid := value.String()
    //fmt.Printf("%+v\n",hostid)
    arrip := strings.Split(hostid,"//")
    //arrip := strings.Split(arr[1],':')
    results[key] = arrip[1]
    //fmt.Printf("%+v\n",results)
  }
  return results
}

func main() {
        var brokers string
	var topic string
	var max_partition int32
	result := getBroker()
	brokerPtr := flag.String("broker", "", "a String")
	topicPtr := flag.String("topic","syslog","a String")
	flag.Parse()
	topic = *topicPtr
	partitions := getPartition(topic)
	max_partition = int32(len(partitions))
	brokers = strings.Join(result,",")
	if *brokerPtr != "" {
	  brokers = *brokerPtr
	}
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": brokers})
	if err != nil {
		panic(err)
	}
	fmt.Println("Connect brokers " + brokers);
	fmt.Printf("Connect partition %+v\n", partitions);
	defer p.Close()

	// Delivery report handler for produced messages
	go func() {
		partitions := getPartition(topic)
		max_partition = int32(len(partitions))
		fmt.Printf("Update max partition: %d\n", max_partition)
		for e := range p.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
				} else {
					fmt.Printf("Delivered message to %v\n", ev.TopicPartition)
				}
			}
		}
	}()

	// Produce messages to topic (asynchronously)
	//topic := "syslog"
	reader := bufio.NewReader(os.Stdin)
	var n int32
	n = 0
	for {
	  word, err := reader.ReadBytes('\n')
	  str_word := string(word[:])
	  arr_str_word := strings.Split(str_word, "\n")
	  str_word = arr_str_word[0]
	  if len(word) != 0 {
	    /*
            if isln == byteln {
              word2 = word[:len(word)-1]
            }else{
              word2 = word
              fmt.Printf("%+v",word)
            }
	    */
	    word2 := str_word
            //fmt.Println(word2)
            if n >= max_partition {
              n = 0
            }
            p.Produce(&kafka.Message{
              TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
	      //TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: n},
              Value: []byte(word2),
            },nil)
            n++
	  }else{
            if err != nil {
              if err != io.EOF {
                  fmt.Println(err)
              }
              break
            }
	  }
	}

	// Wait for message deliveries
	p.Flush(60 * 1000)
}
