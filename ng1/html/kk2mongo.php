<?php
function microtime_float(){
  list($usec,$sec) = explode(" ",microtime());
  return $usec + $sec;
}

function getbroker($zookeeper="zookeeper:2181"){
  $zk = new zookeeper($zookeeper);
  $path = "/brokers/ids";
  $r = $zk->getchildren($path);
  foreach($r as $v){
    $key = $path."/".$v;
    $value = $zk->get($key);
    $json = json_decode($value);
    $hostname =  $json->endpoints[0];
    if(preg_match("/[A-Z]+:\/\/([0-9a-f]+):([0-9]+)/",$hostname,$arr_result)){
      list($dc,$host,$port) = $arr_result;
      $arr[] = "$host:$port";
    }
  }
  return $arr;
}
$conf = new RdKafka\Conf();

// Set the group id. This is required when storing offsets on the broker
$conf->set('group.id', 'myConsumerGroup3');

$rk = new RdKafka\Consumer($conf);
$arr_broker = getbroker();
foreach($arr_broker as $v){
	if($rk->addBrokers($v)){
	  //echo "Add broker $v success.\n";
	}
}
//$rk->addBrokers("ff820d365ea1,1669b5f567b9,f6094acd8f95");

$topicConf = new RdKafka\TopicConf();
$topicConf->set('auto.commit.interval.ms', 100);

// Set the offset store method to 'file'
$topicConf->set('offset.store.method', 'file');
$topicConf->set('offset.store.path', sys_get_temp_dir());

// Alternatively, set the offset store method to 'broker'
// $topicConf->set('offset.store.method', 'broker');

// Set where to start consuming messages when there is no initial offset in
// offset store or the desired offset is out of range.
// 'smallest': start from the beginning
$topicConf->set('auto.offset.reset', 'smallest');

$topic = $rk->newTopic("syslog", $topicConf);
$queue = $rk->newQueue();
// Start consuming partition 0
$topic->consumeQueueStart($argv[1], RD_KAFKA_OFFSET_STORED,$queue);
if(isset($argv[2]))
  $topic->consumeQueueStart($argv[2], RD_KAFKA_OFFSET_STORED,$queue);
if(isset($argv[3]))
  $topic->consumeQueueStart($argv[3], RD_KAFKA_OFFSET_STORED,$queue);
$timestamp = microtime_float();                                                
$count = 0;
$reset = false;
while (true) {
    //$message = $topic->consume(0, 120*10000);
    $message = $queue->consume(1000);
    switch ($message->err) {
        case RD_KAFKA_RESP_ERR_NO_ERROR:
            //var_dump($message);
            if($reset){                    
              $timestamp = microtime_float();          
            }
	    if($message->payload){
	      echo $message->payload."\n";
		    $count++;
		    $reset = false;
	    }
            break;
        case RD_KAFKA_RESP_ERR__PARTITION_EOF:
            //echo "No more messages; will wait for more\n";
            $timestampOld = $timestamp;                                                        
            $timestamp = microtime_float();                                           
            $diff = $timestamp - $timestampOld;
            //echo $diff." sec.\n";                             
	    //echo $count." msg.\n";
	    $reset = true;
            //$count = 0; 
            break;
        case RD_KAFKA_RESP_ERR__TIMED_OUT:
            //echo "Timed out\n";
            break;
        default:
            throw new \Exception($message->errstr(), $message->err);
            break;
    }
}

?>

