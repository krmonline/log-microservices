#!/usr/local/bin/php
<?php
function getpartition($topic){
  $zk = new zookeeper("zookeeper:2181");
  $path = "/brokers/topics/$topic/partitions";
  $r = $zk->getchildren($path);
  return $r;
}


$topic_text = "syslog";

$stdin = fopen("php://stdin","r");


$event_count = isset($argv[1])?$argv[1]:1000;
$rk = new RdKafka\Producer();
$rk->setLogLevel(LOG_DEBUG);
$rk->addBrokers("kafka");

$topic = $rk->newTopic($topic_text);
$rr = getpartition($topic_text);
$n = 0;
if($max_partition = count($rr)){
  $part = $rr[$n];
}else{
  die("Please recheck topic $topic thare is no partition with message $rr");
}

while(!feof($stdin)){
    $msg = fgets($stdin);
    if($n >= $max_partition){
        $n = 0;
    }
    $topic->produce($rr[$n],0,$msg);
    $rk->poll(0);
    $n++;

}

while ($rk->getOutQLen() > 0) {
    $rk->poll(50);
}

?>
