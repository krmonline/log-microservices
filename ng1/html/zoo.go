package main
//Version 0.1
//Create by Chakrit<chakrit@softnix.co.th>

import (
	"fmt"
	"time"
	"github.com/go-zookeeper/zk"
	"os"
)
var dir string
func main() {
	dir = os.Args[1]
	c, _, err := zk.Connect([]string{"zookeeper"}, time.Second) //*10)
	if err != nil {
		panic(err)
	}
	children, stats, _ , err := c.ChildrenW(dir)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n",stats)
	for _, element := range children {
		fmt.Printf("%+v\n", element)
	}
	results_byte,_, err := c.Get(dir)
	if err != nil {
                panic(err)
        }
        s := string(results_byte[:])
	fmt.Printf("%+v\n",s)
}
