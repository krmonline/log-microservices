<?php
//ini_set("date.timezone","Asia/Bangkok");
header('Access-Control-Allow-Origin: *');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require '../vendor/autoload.php';
require 'config.php';
$app = new \Slim\App(['settings' => $config]);


$app->get('/', function () {
    $name = "/srcip\n/dstip";
    return $name;
});

$app->get('/q', function (Request $request, Response $response, array $args) {
  //var_dump($_GET);
  $mongoip = $this->get('settings')['mongo_server'];

  //$response->getBody()->write("Source IP $srcip");
  $manager = new MongoDB\Driver\Manager("mongodb://$mongoip");


  //Query
  //date
  $from = isset($_GET['from'])?strtotime($_GET['from']):"";

  if($from){
    $to = (isset($_GET['to']) && $_GET['to'])?strtotime($_GET['to']):$from+86400;
    $filter["Timestamp"] = ['$gte' => "$from" , '$lte' => "$to"];
    $month_f = date("M",$from);
    $month_t = date("M",$to);
    if($month_f != $month_t){
      //var_dump($month_f);
      die("from and to seperate month can not query");
    }
    $month = strtolower($month_f);
  }else{
    echo "Require from parameter.";
  }

  //source ip
  $srcip = isset($_GET['srcip'])?$_GET['srcip']:"";
  $filter["Framed-IP-Address"] =  $srcip;

  //var_dump($filter);
  //limit skip
  $limit = isset($_GET["limit"])?$_GET["limit"]:$this->get('settings')['limit'];
  $page = isset($_GET["page"])?$_GET["page"]:"0";
  $skip = $page*$limit;
  $options = ["limit" => $limit,"skip" => $skip,
    'projection' => [
      'Framed-IP-Address' => 1,
      'Calling-Station-Id' => 1,
      'Timestamp' => 1,
      'Acct-Status-Type' => 1
    ]
  ];

  $query = new MongoDB\Driver\Query($filter, $options);
  $cursor = $manager->executeQuery('radius.'.$month, $query);

  foreach($cursor as $doc){
    $result[] = $doc;
  }
  if(!isset($result)){
    return "[]";
  }
  return $response->withJson($result);
});
$app->run();
