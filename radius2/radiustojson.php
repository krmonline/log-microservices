<?php
$separate = "@@@";
$id = isset($argv[1])?$argv[1]:"";

$data = json_decode($argv[2]);
//print_r($data);
$user = $data->user;
$uuid = $data->uuid;
$type_log = $data->type_log;
//die();
$wlog = function($text) {
    if(!$text){
        return false;
    }
    $filename = "/var/log/softnix/indexing.log";
    $time = date("Y-m-d H:i:s");
    $text = $time."\t".$text;
    $text = escapeshellarg($text);
    `echo $text >> $filename`;
};

$stdin = fopen('php://stdin', 'r');
$reset = false;
$text = "";
while (!feof($stdin)) {
    $line = fgets($stdin);
    $line = trim($line);
    if ($line) {
        $text = "";
        $text .= "\"user\":\"$user\",";
        $text .= "\"uuid\":\"$uuid\",";
        $text .= "\"type_log\":\"$type_log\"";
        $arr = explode($separate,$line);
	$tmp = array_shift($arr);
        //var_dump($arr);
        foreach($arr as $v) {
            list($radius_key,$radius_value) = explode("=",$v);
            $radius_key = trim($radius_key);
            $radius_value = trim($radius_value);
            if(preg_match("/\"([^\"]+)\"/",$radius_value,$pregResult)) {
                //var_dump($pregResult);
                $radius_value = $pregResult[1];
                //echo $radius_value." is new value\n";
            }
            if($radius_key && $radius_value){
                $text .= ($text)?",":"";
                if ($radius_key == "3GPP-Negotiated-DSCP") {
                    $radius_value = "";
                }
                $text .= "\"$radius_key\":\"$radius_value\"";
            }
        }
        $json = json_decode("{".$text."}");
        echo "".json_encode($json)."\n";
    }
}
?>
