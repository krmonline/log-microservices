<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <title>Hello, world!</title>
  </head>
  <body>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>


    <script>
    function timeConverter(UNIX_timestamp){
      var a = new Date(UNIX_timestamp * 1000);
      var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
      var year = a.getFullYear();
      var month = months[a.getMonth()];
      var date = a.getDate();
      var hour = a.getHours();
      var min = "0"+a.getMinutes();
      var sec = "0"+a.getSeconds();
      var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min.substr(-2) + ':' + sec.substr(-2) ;
      return time;
    }

    $(document).ready(function() {
      var radiustable = $('#radiustable').DataTable({
        columns: [
          {data: "time"},
          {data: "Framed-IP-Address"},
          {data: "Calling-Station-Id"},
          {data: "Acct-Status-Type"}
        ]
      });
      var firewalltable = $('#firewalltable').DataTable({
        columns: [
          {data: "time2"},
          {data: "srcip1"},
          {data: "srcp"},
          {data: "dst"},
          {data: "dstp"}
        ]
      });
      var mergetable = $('#mergetable').DataTable({
        columns: [
          {data: "time2"},
          {data: "srcip1"},
          {data: "srcp"},
          {data: "dst"},
          {data: "dstp"},
          {data: "username"},
        ]
      });
    } );
    </script>
    <div class="container">
    	<div class="row">
        <div class="navbar-brand">LogQuery</div>
        <div class="card">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#radiuslog" aria-controls="home" role="tab" data-toggle="tab">Radius Log</a></li>
            <li role="presentation"><a href="#firewalllog" aria-controls="profile" role="tab" data-toggle="tab">Firewall Log</a></li>
            <li role="presentation"><a href="#mergelog" aria-controls="messages" role="tab" data-toggle="tab">Merge Log</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="radiuslog">
              <div class="col-md-12">
              <h4>Radius Log</h4>
              <div>
                <form action="#">
                  <div class="form-group">
                    <label for="frameIPAddress">Framed-IP-Address:</label>
                    <input type="text" class="form-control" id="frameIPAddress" value="10.211.65.220">
                  </div>
                  <div class="form-group">
                    <label for="callingStationId">Calling-Station-Id:</label>
                    <input type="text" class="form-control" id="callingStationId">
                  </div>
                  <div class="form-group">
                    <label for="datetimepickerRD1">From:</label>
                    <div class='input-group date' id='datetimepickerRD1'>
                        <input type='text' class="form-control" id="RDdateFrom"  value="03/18/2014 12:00 AM"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <label for="datetimepickerRD2">To:</label>
                    <div class='input-group date' id='datetimepickerRD2'>
                        <input type='text' class="form-control" id="RDdateTo" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <button type="button" class="btn btn-sm btn-success" id="btnRDok">OK</button>
                  <script type="text/javascript">
                      $(function () {
                          $('#datetimepickerRD1').datetimepicker();
                          $('#datetimepickerRD2').datetimepicker();
                          $('#btnRDok').click(function (){
                            dateFrom = $('#RDdateFrom').val();
                            dateTo = $('#RDdateTo').val();
                            frameIPAddress = $('#frameIPAddress').val();
                            //alert(frameIPAddress);
                            $.ajax({
                              url: "http://localhost:8083/q",
                              type: 'GET',
                              data: {
                                  "srcip" : frameIPAddress,
                                  "from" : dateFrom,
                                  "to" : dateTo
                              } ,
                              success: function(result){
                                //alert(result.length);
                                if(result == '[]'){
                                  radiustable = $('#radiustable').DataTable();
                                  radiustable.clear();
                                  radiustable.draw();
                                }else{
                                  //console.log(result[0].Timestamp);
                                  var result2;
                                  for(i=0;i<result.length;i++){
                                    result[i].time = timeConverter(result[i].Timestamp);
                                  }
                                  radiustable = $('#radiustable').DataTable();
                                  radiustable.clear();
                                  radiustable.rows.add(result);
                                  radiustable.draw();
                                }
                              },
                              error: function(XMLHttpRequest, textStatus, errorThrown) {
                                  alert("Status: " + textStatus); alert("Error: " + errorThrown);
                              }
                            });
                          })
                      });
                  </script>
                </form>
              </div>
              <br>
              <div class="table-responsive">
                <table id="radiustable" class="table table-striped" width="100%">
                  <thead>
                    <th>Timestamp</th>
                    <th>Framed-IP-Address</th>
                    <th>Calling-Station-Id</th>
                    <th>Acct-Status-Type</th>
                  </thead>
                </table>
              </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="firewalllog">
              <div class="col-md-12">
              <h4>Firewall Log</h4>
              <div>
                <form action="#">
                  <div class="col-md-4">
                    <label for="srcip">Source IP:</label>
                    <input type="text" class="form-control" id="srcip" value="10.40.218.82">
                  </div>
                  <div class="col-md-2">
                    <label for="srcp">Port:</label>
                    <input type="text" class="form-control" id="srcp">
                  </div>
                  <div class="col-md-4">
                    <label for="dstip">Destination IP:</label>
                    <input type="text" class="form-control" id="dstip">
                  </div>
                  <div class="col-md-2">
                    <label for="dstp">Port:</label>
                    <input type="text" class="form-control" id="dstp">
                  </div>
                  <div class="col-md-12">
                    <label for="datetimepickerFW1">From:</label>
                    <div class='input-group date' id='datetimepickerFW1'>
                        <input type='text' class="form-control" id="FWdateFrom"  value="11/10/2017 1:00 PM"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <label for="datetimepickerFW2">To:</label>
                    <div class='input-group date' id='datetimepickerFW2'>
                        <input type='text' class="form-control" id="FWdateTo" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <div class="col-md-12"><br>
                  </div>
                  <div class="col-md-12">
                    <button type="button" class="btn btn-sm btn-success" id="btnFWok">OK</button>
                  </div>

                  <script type="text/javascript">
                      $(function () {
                          $('#datetimepickerFW1').datetimepicker();
                          $('#datetimepickerFW2').datetimepicker();
                          $('#btnFWok').click(function (){
                            //alert("FW Active");
                            dateFrom = $('#FWdateFrom').val();
                            dateTo = $('#FWdateTo').val();
                            srcip = $('#srcip').val();
                            srcp = $('#srcp').val();
                            dstip = $('#dstip').val();
                            dstp = $('#dstp').val();
                            //alert(frameIPAddress);
                            $.ajax({
                              url: "http://localhost:8081/q",
                              type: 'GET',
                              data: {
                                  "srcip" : srcip,
                                  "srcp": srcp,
                                  "dstip": dstip,
                                  "dstp": dstp,
                                  "from" : dateFrom,
                                  "to" : dateTo
                              } ,
                              success: function(result){
                                if(result == '[]'){
                                  radiustable = $('#firewalltable').DataTable();
                                  radiustable.clear();
                                  radiustable.draw();
                                }else{
                                  var result2;
                                  for(i=0;i<result.length;i++){
                                    result[i].time2 = timeConverter(result[i].timestamp);
                                  }
                                  firewalltable = $('#firewalltable').DataTable();
                                  firewalltable.clear();
                                  firewalltable.rows.add(result);
                                  firewalltable.draw();
                                }
                              },
                              error: function(XMLHttpRequest, textStatus, errorThrown) {
                                  alert("Status: " + textStatus); alert("Error: " + errorThrown);
                              }
                            });
                          })
                      });
                  </script>
                </form>
              </div>
              <br>
              <div class="row">
                <table id="firewalltable" class="table table-striped" width="100%">
                  <thead>
                    <th>Timestamp</th>
                    <th>Source IP</th>
                    <th>Source Port</th>
                    <th>Destination IP</th>
                    <th>Destination Port</th>
                  </thead>
                </table>
              </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="mergelog">
              <div class="col-md-12">
              <h4>Merge Log</h4>
              <div>
                <form action="#">
                  <div class="col-md-4">
                    <label for="MGsrcip">Source IP:</label>
                    <input type="text" class="form-control" id="MGsrcip" value="10.40.218.82">
                  </div>
                  <div class="col-md-2">
                    <label for="MGsrcp">Port:</label>
                    <input type="text" class="form-control" id="MGsrcp">
                  </div>
                  <div class="col-md-4">
                    <label for="MGdstip">Destination IP:</label>
                    <input type="text" class="form-control" id="MGdstip" value="49.231.61.74">
                  </div>
                  <div class="col-md-2">
                    <label for="MGdstp">Port:</label>
                    <input type="text" class="form-control" id="MGdstp">
                  </div>
                  <div class="col-md-12">
                    <label for="datetimepickerMG1">From:</label>
                    <div class='input-group date' id='datetimepickerMG1'>
                        <input type='text' class="form-control" id="MGdateFrom"  value="11/10/2017 1:00 PM"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <label for="datetimepickerMG2">To:</label>
                    <div class='input-group date' id='datetimepickerMG2'>
                        <input type='text' class="form-control" id="MGdateTo" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <div class="col-md-12"><br>
                  </div>
                  <div class="col-md-12">
                    <button type="button" class="btn btn-sm btn-success" id="btnMGok">OK</button>
                  </div>

                  <script type="text/javascript">
                      $(function () {
                          $('#datetimepickerMG1').datetimepicker();
                          $('#datetimepickerMG2').datetimepicker();
                          $('#btnMGok').click(function (){
                            //alert("FW Active");
                            dateFrom = $('#MGdateFrom').val();
                            dateTo = $('#MGdateTo').val();
                            srcip = $('#MGsrcip').val();
                            srcp = $('#MGsrcp').val();
                            dstip = $('#MGdstip').val();
                            dstp = $('#MGdstp').val();
                            //alert(frameIPAddress);
                            $.ajax({
                              url: "http://localhost:8081/u",
                              type: 'GET',
                              data: {
                                  "srcip" : srcip,
                                  "srcp": srcp,
                                  "dstip": dstip,
                                  "dstp": dstp,
                                  "from" : dateFrom,
                                  "to" : dateTo
                              } ,
                              success: function(result){
                                if(result == '[]'){
                                  mergetable = $('#mergetable').DataTable();
                                  mergetable.clear();
                                  mergetable.draw();
                                }else{
                                  var result2;
                                  for(i=0;i<result.length;i++){
                                    result[i].time2 = timeConverter(result[i].timestamp);
                                  }
                                  mergetable = $('#mergetable').DataTable();
                                  mergetable.clear();
                                  mergetable.rows.add(result);
                                  mergetable.draw();
                                }
                              },
                              error: function(XMLHttpRequest, textStatus, errorThrown) {
                                  alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                              }
                            });
                          })
                      });
                  </script>
                </form>
              </div>
              <br>
              <div class="row">
                <table id="mergetable" class="table table-striped" width="100%">
                  <thead>
                    <th>Timestamp</th>
                    <th>Source IP</th>
                    <th>Source Port</th>
                    <th>Destination IP</th>
                    <th>Destination Port</th>
                    <th>Calling-Station-Id:</th>
                  </thead>
                </table>
              </div>
              </div>
            </div>
        </div>
</div>

    	</div>
    </div>

  </body>
</html>
